<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://kszczygiel9.pl
 * @since      1.0.0
 *
 * @package    Promoted_Product
 * @subpackage Promoted_Product/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Promoted_Product
 * @subpackage Promoted_Product/admin
 * @author     Krzysztof <krzysiek.szczygiel96@gmail.com>
 */
class Promoted_Product_Admin
{

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct($plugin_name, $version)
	{
		$this->plugin_name = $plugin_name;
		$this->version = $version;


		add_filter('woocommerce_get_sections_products', [$this, 'wc_promoted_product_add_section']);
		add_filter('woocommerce_get_settings_products', [$this, 'wc_promoted_product_settings'], 10, 2);

		add_action('woocommerce_product_options_general_product_data', [$this, 'add_custom_fields']);
		add_action('woocommerce_process_product_meta', [$this, 'save_custom_fields']);

		add_action('promotion_expiry', [$this,'scheduledDeletionChecker']);
	}


	/**
	 * Scheduled check to delete promoted product if expired
	 *
	 * @since    1.0.0
	 * @return 			void
	 */
	public function scheduledDeletionChecker(){

		$promoted = Promoted_Product::get_promoted_product();
		$is_scheduled = get_post_meta($promoted->ID, 'set_expiration', true);
		if($is_scheduled == 'yes'){
			if(current_time('timestamp') > get_post_meta($promoted->ID, 'expiration_datetime', true)){
				self::deactivate_current_product_promotion();
			}
		}

	}


	/**
	 * Register promoted product woocommerce section
	 *
	 * @since    1.0.0
	 * @param      array    $sections       Woocommerce sections value
	 * @return 			array 	$sections       Woocommerce sections value
	 */
	public function wc_promoted_product_add_section($sections)
	{

		$sections['promoted-product'] = __('Promoted Product', 'promoted-product');

		return $sections;
	}


	/**
	 * Register promoted product woocommerce section
	 *
	 * @since    1.0.0
	 * @param      array    $settings       Woocommerce sections fields array
	 * @param 			string 	$current_section       Woocommerce settings section slug
	 * @return 			array    $settings       Woocommerce modified sections fields array
	 */
	public function wc_promoted_product_settings($settings, $current_section)
	{

		if ($current_section == 'promoted-product') {

			$settings = $this->get_base_WC_settings();

			$promoted = Promoted_Product::get_promoted_product();

			if ($promoted) {

				$edit_link = get_edit_post_link($promoted->ID);
				$title = $promoted->post_title;

				$settings[] =	array(
					'title' => __('Currently promoted:', 'promoted-product'),
					'type' => 'title',
					'desc' => '<a href="' . $edit_link . '">' . $title . '</a>',
					'id' => 'promoted_product_settings',
				);
			}

			$settings[] = array('type' => 'sectionend', 'id' => 'promoted_product_settings');
		}

		return $settings;
	}

	/**
	 * Register promoted product woocommerce section
	 *
	 * @since    1.0.0
	 * @return 			array    $settings       Woocommerce promoted product base settings
	 */
	private function get_base_WC_settings()
	{
		return array(
			array(
				'title' => __('Promoted Product Settings', 'promoted-product'),
				'type' => 'title',
				'desc' => __('Configure settings for the promoted product.', 'promoted-product'),
				'id' => 'promoted_product_settings',
			),

			array(
				'title' => __('Promoted Product Title', 'promoted-product'),
				'desc' => __('Enter the title for the promoted product section.', 'promoted-product'),
				'id' => 'promoted_product_title',
				'type' => 'text',
				'default' => __('Promoted Product:', 'promoted-product'),
				'desc_tip' => true,
			),
			array(
				'title' => __('Background Color', 'promoted-product'),
				'desc' => __('Select the background color for the promoted product section.', 'promoted-product'),
				'id' => 'promoted_product_background_color',
				'type' => 'color',
				'default' => '#ff0000',
				'desc_tip' => true,
			),
			array(
				'title' => __('Text Color', 'promoted-product'),
				'desc' => __('Select the text color for the promoted product section.', 'promoted-product'),
				'id' => 'promoted_product_text_color',
				'type' => 'color',
				'default' => '#ffffff',
				'desc_tip' => true,
			),
		);
	}


		/**
	 * Register promoted product single post edit fields
	 *
	 * @since    1.0.0
	 * @return 			void  
	 */
	public function add_custom_fields()
	{
		woocommerce_wp_checkbox(array(
			'id' => 'promoted_product',
			'label' => __('Promote this product', 'promoted-product'),
			'desc_tip' => true,
			'description' => __('Check this box to mark this product as promoted.', 'promoted-product'),
		));

		woocommerce_wp_text_input(array(
			'id' => 'custom_title',
			'label' => __('Custom Title', 'promoted-product'),
			'desc_tip' => true,
			'description' => __('Enter a custom title to be shown instead of the product title.', 'promoted-product'),
		));

		woocommerce_wp_checkbox(array(
			'id' => 'set_expiration',
			'label' => __('Set expiration date and time', 'promoted-product'),
			'desc_tip' => true,
			'description' => __('Check this box to set an expiration date and time for the promotion.', 'promoted-product'),
		));

		echo '<div class="options_group" id="expiration_fields">';
		woocommerce_wp_text_input(array(
			'id' => 'expiration_datetime',
			'label' => __('Expiration Date and Time', 'promoted-product'),
			'type' => 'datetime-local',
			'desc_tip' => true,
			'description' => __('Select the date and time when the promotion will expire.', 'promoted-product'),
		));
		echo '</div>';
	}


	/**
	 * Register promoted product woocommerce section
	 *
	 * @since    1.0.0
	 * @return 			void  
	 */
	static function deactivate_current_product_promotion()
	{
		$promoted = Promoted_Product::get_promoted_product();
		update_post_meta($promoted->ID, 'promoted_product', 'no');
	}


		/**
	 * Register promoted product woocommerce section
	 *
	 * @since    1.0.0
	 * @param      array    $product_id       Current product ID
	 * @return 			void
	 */
	public function save_custom_fields($product_id)
	{
		$promoted_product = isset($_POST['promoted_product']) ? 'yes' : 'no';
		$set_expiration = isset($_POST['set_expiration']) ? 'yes' : 'no';

		//deactivate active promotion in order to make current product the only one that's being promoted
		if ($promoted_product  == 'yes') {
			self::deactivate_current_product_promotion();
		}

		update_post_meta($product_id, 'promoted_product', $promoted_product);

		if (isset($_POST['custom_title'])) {
			update_post_meta($product_id, 'custom_title', sanitize_text_field($_POST['custom_title']));
		}

		update_post_meta($product_id, 'set_expiration', $set_expiration);

		if ($set_expiration == 'yes') {
			update_post_meta($product_id, 'expiration_datetime', sanitize_text_field($_POST['expiration_datetime']));
		} else {
			delete_post_meta($product_id, 'expiration_datetime');
		}
	}
}
