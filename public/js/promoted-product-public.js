document.addEventListener('DOMContentLoaded', function () {
  const header = document.querySelector('header');
  const xhr = new XMLHttpRequest();
  xhr.open('POST', promoted_product.ajax_url, true);
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

  xhr.onload = () => {
    if (xhr.status == 200) {
      const response = JSON.parse(xhr.responseText);
      if (response.success) {
        const data = response.data;
        const promotedContent = document.createElement('div');
        promotedContent.innerHTML = `
          <div class="promoted-product-banner" style="background-color: ${data.bg_color}; color: ${data.txt_color}; padding: 10px; text-align: center; font-weight: bold;">
            ${data.heading}: <a style="color: ${data.txt_color};" href="${data.prod_link}">${data.title}</a>
          </div>`;
        header.appendChild(promotedContent);
      } else {
        console.error('Error fetching promoted product data:', response.data);
      }
    } else {
      console.error('Error fetching promoted product data:', xhr.statusText);
    }
  };

  xhr.onerror = () => {
    console.error('Request failed');
  };

  xhr.send('action=get_promoted_product');
});