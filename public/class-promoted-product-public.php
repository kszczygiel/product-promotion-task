<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://kszczygiel9.pl
 * @since      1.0.0
 *
 * @package    Promoted_Product
 * @subpackage Promoted_Product/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Promoted_Product
 * @subpackage Promoted_Product/public
 * @author     Krzysztof <krzysiek.szczygiel96@gmail.com>
 */
class Promoted_Product_Public
{

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct($plugin_name, $version)
	{

		$this->plugin_name = $plugin_name;
		$this->version = $version;


		add_action('wp_ajax_get_promoted_product', [$this, 'get_promoted_product']);
		add_action('wp_ajax_nopriv_get_promoted_product', [$this, 'get_promoted_product']);
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles()
	{

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Promoted_Product_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Promoted_Product_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/promoted-product-public.css', array(), $this->version, 'all');
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts()
	{

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Promoted_Product_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Promoted_Product_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/promoted-product-public.js', array('jquery'), $this->version, false);
		wp_localize_script($this->plugin_name, 'promoted_product', array('ajax_url' => admin_url('admin-ajax.php')));
	}

	/**
	 * Scheduled check to delete promoted product if expired
	 *
	 * @since    1.0.0
	 * @param 	object post object of currently promoted product
	 * @return 			array of all needed settings to build banner
	 */
	private function getPromotedData($promoted)
	{
		$title = get_post_meta($promoted->ID, 'custom_title', true) ?: $promoted->post_title;

		return array(
			'title' => $title,
			'heading' => get_option('promoted_product_title') ?? false,
			'bg_color' => get_option('promoted_product_background_color') ?? false,
			'txt_color' => get_option('promoted_product_text_color') ?? false,
			'prod_link' => get_permalink($promoted),
		);
	}



	public function get_promoted_product()
	{
		$promoted = Promoted_Product::get_promoted_product();
		if (!$promoted) {
			wp_send_json_error('Missing data');
		}

		wp_send_json_success($this->getPromotedData($promoted));

		wp_die();
	}
}
