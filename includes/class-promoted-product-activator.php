<?php

/**
 * Fired during plugin activation
 *
 * @link       https://kszczygiel9.pl
 * @since      1.0.0
 *
 * @package    Promoted_Product
 * @subpackage Promoted_Product/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Promoted_Product
 * @subpackage Promoted_Product/includes
 * @author     Krzysztof <krzysiek.szczygiel96@gmail.com>
 */
class Promoted_Product_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
