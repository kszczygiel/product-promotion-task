<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://kszczygiel9.pl
 * @since             1.0.0
 * @package           Promoted_Product
 *
 * @wordpress-plugin
 * Plugin Name:       Promoted product
 * Plugin URI:        https://kszczygiel9.pl
 * Description:       This is a description of the plugin.
 * Version:           1.0.0
 * Author:            Krzysztof
 * Author URI:        https://kszczygiel9.pl/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       promoted-product
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define('PROMOTED_PRODUCT_VERSION', '1.0.0');

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-promoted-product-activator.php
 */
function activate_promoted_product()
{
	require_once plugin_dir_path(__FILE__) . 'includes/class-promoted-product-activator.php';
	Promoted_Product_Activator::activate();

	if (!wp_next_scheduled('promotion_expiry')) {
		wp_schedule_event(time(), 'halfhourly', 'promotion_expiry');
	}
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-promoted-product-deactivator.php
 */
function deactivate_promoted_product()
{
	require_once plugin_dir_path(__FILE__) . 'includes/class-promoted-product-deactivator.php';
	Promoted_Product_Deactivator::deactivate();
}


register_activation_hook(__FILE__, 'activate_promoted_product');
register_deactivation_hook(__FILE__, 'deactivate_promoted_product');

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path(__FILE__) . 'includes/class-promoted-product.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_promoted_product()
{

	if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
		$plugin = new Promoted_Product();
		$plugin->run();
	}
}

run_promoted_product();
